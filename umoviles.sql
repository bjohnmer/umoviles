-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 10, 2013 at 09:44 AM
-- Server version: 5.5.32-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `umoviles`
--

-- --------------------------------------------------------

--
-- Table structure for table `conductores`
--

CREATE TABLE IF NOT EXISTS `conductores` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla de Conductores',
  `cedula` varchar(12) NOT NULL COMMENT 'Cédula o Documento de identidad del conductor',
  `nombres` varchar(75) NOT NULL COMMENT 'Primer y segundo nombre del conductor',
  `apellidos` varchar(75) NOT NULL COMMENT 'Primer y segundo apellido del conductor',
  `direccion` text NOT NULL COMMENT 'Dirección de habitación del conductor',
  `telf` varchar(50) DEFAULT NULL COMMENT 'Teléfono del conductor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Información de los conductores de las unidades retenidas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `retenciones`
--

CREATE TABLE IF NOT EXISTS `retenciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Campo clave de la tabla de retenciones',
  `fecha` date NOT NULL COMMENT 'Fecha de la retención',
  `unidades_id` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de Unidades',
  `conductores_id` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de Conductores',
  `condiciones` text NOT NULL COMMENT 'Condiciones de la unidad retenida',
  `cretencion` enum('293','Casco','Documentación','Otro') NOT NULL DEFAULT '293' COMMENT 'Causa de la retención',
  `dretencion` text NOT NULL COMMENT 'Detalle de la retención',
  `estatus` enum('Devuelta[293]','Tránsito','Fiscalía') NOT NULL DEFAULT 'Devuelta[293]' COMMENT 'Estatus de la unidad',
  `imagen` varchar(255) DEFAULT NULL COMMENT 'Foto de la unidad retenida',
  `licencia` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación de Licencia de Conducir',
  `cmedica` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación de la Carta Médica',
  `rcivil` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación del Seguro de Responsabilidad Civil',
  `cmotorizado` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación del Carnet de Motorizado',
  `corigen` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y Validación del Certificado de Orígen',
  PRIMARY KEY (`id`),
  KEY `fk_retenciones_unidades` (`unidades_id`),
  KEY `fk_retenciones_conductores1` (`conductores_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Histórico de retetenciones de las unidades móviles' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `unidades`
--

CREATE TABLE IF NOT EXISTS `unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Campo clave de la tabla de unidades',
  `marca` varchar(45) NOT NULL COMMENT 'Marca de la moto',
  `modelo` varchar(45) NOT NULL COMMENT 'Modelo de la moto',
  `anio` int(11) NOT NULL COMMENT 'Año de la moto',
  `smotor` varchar(45) NOT NULL COMMENT 'Serial de motor',
  `scarroceria` varchar(45) NOT NULL COMMENT 'Serial de carrocería',
  `color` varchar(45) NOT NULL COMMENT 'Color de la moto',
  `placa` varchar(45) DEFAULT NULL COMMENT 'Placa de la moto',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de nidades retenidasu' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `unidades`
--

INSERT INTO `unidades` (`id`, `marca`, `modelo`, `anio`, `smotor`, `scarroceria`, `color`, `placa`) VALUES
(1, 'jkdsjhfkhsdk', 'kjhkas', 2000, 'mnbmbnvmxzb', 'mncbxzmcbzx', 'nmbczxmcb', 'mbmbmzxcb');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla de usuarios',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre completo del usuario',
  `login` varchar(20) NOT NULL COMMENT 'Login o nick del usuario',
  `clave` varchar(50) NOT NULL COMMENT 'Clave del usuario',
  `conf` varchar(45) NOT NULL COMMENT 'Campo de confirmación de la clave del usuario',
  `tipo` enum('Administrador','Supervisor') NOT NULL DEFAULT 'Supervisor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de usuarios del sistema' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `login`, `clave`, `conf`, `tipo`) VALUES
(1, 'Oldris Torres', 'oldrIs', '3a0cf8949de18463283d2eb72f0c00e3e4b9311a', '3a0cf8949de18463283d2eb72f0c00e3e4b9311a', 'Administrador'),
(2, 'Supervisor', 'supe', '8cd0ae8dfebb2dd4a28bffe6d2e7e3201ddf73d0', '8cd0ae8dfebb2dd4a28bffe6d2e7e3201ddf73d0', 'Supervisor');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `retenciones`
--
ALTER TABLE `retenciones`
  ADD CONSTRAINT `fk_retenciones_conductores1` FOREIGN KEY (`conductores_id`) REFERENCES `conductores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retenciones_unidades` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
