Aplicación Web para el control de unidades retenidas en la Estación Policial 5.1 Monay © 2013
========
(en desarrollo)

Asesoría: Diseño, Desarrollo e Ingeniería de Software Web

Herramientas utilizadas:

Front-end: HTML5, CSS3, Framework CSS Twitter Bootstrap (http://getbootstrap.com)

Back-end: PHP, Framework PHP Codeigniter (http://ellislab.com/codeigniter), Librería GroceryCRUD (http://www.grocerycrud.com/)

Base de Datos: MySQL