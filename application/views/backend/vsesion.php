    <!-- <img src="<?=base_url()?>assets/img/bg.jpg" alt="" class="img-responsive"> -->
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?=base_url()?>assets/img/img1.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Controla Las Unidades Retenidas</h1>
              <p>Mediante la opción de menú Unidades se pueden registrar las <br> Unidades Motorizadas retenidas</p>
              <!-- <p><a class="btn btn-large btn-primary" href="#">Sign up today</a></p> -->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?=base_url()?>assets/img/img2.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Registre los Conductores</h1>
              <p>Recuerde registrar los conductores antes de hacer una retención</p>
              <!-- <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p> -->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?=base_url()?>assets/img/img3.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Estadísticas al instante</h1>
              <p>En el menú estadísticas podrá ver el historial detallado de las retenciones hechas en un periodo de tiempo.</p>
              <!-- <p><a class="btn btn-large btn-primary" href="#">Browse gallery</a></p> -->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span></a>
    </div><!-- /.carousel -->
