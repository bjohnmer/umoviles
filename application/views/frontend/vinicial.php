<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Unidades Motorizadas Retenidas</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/css/jumbotron.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>assets/js/html5shiv.js"></script>
      <script src="<?=base_url()?>assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <div class="navbar navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Unidades Motorizadas Retenidas</a>
          </div>
          <div class="navbar-collapse collapse" style="height: 1px;">
            <form class="navbar-form navbar-right" action="<?=base_url()?>inicial/autenticacion" method="post">
              <div class="form-group">
                <input type="text" class="form-control" name="login" placeholder="Usuario(Login)" value="<?=set_value("login")?>">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="clave" placeholder="Clave">
              </div>
              <button class="btn btn-success" type="submit">Entrar</button>
            </form>
          </div><!--/.navbar-collapse -->
        </div>
      </div>
  
      <?php if (!empty($mensaje)): ?>
        <div class="alert alert-<?=$mensaje['tipo']?> alert-dismissable" style="margin-bottom:0;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?=$mensaje['mensaje']?>
        </div>
      <?php endif ?>

    <img src="<?=base_url()?>assets/img/front.jpg" alt="" class="img-responsive">
    <footer>
      <div class="col-lg-12">
        <h5>Aplicación Web para el control de unidades retenidas en la Estación Policial 5.1 Monay &copy; 2013</h5>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>assets/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
  </body>
</html>
