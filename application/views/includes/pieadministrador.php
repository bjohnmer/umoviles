
    <footer>
      <div class="col-lg-12">
        <h5>Aplicación Web para el control de unidades retenidas en la Estación Policial 5.1 Monay &copy; 2013</h5>
      </div>
    </footer>


  <?php if (!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
  <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
  <?php endif ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
  <?php if (empty($js_files)): ?>
    <script src="<?=base_url()?>assets/js/jquery.js"></script>
  <?php endif ?>

    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/holder.js"></script>
    <script>
      $('.carousel').carousel({
        interval: 5000
      })
    </script>
  </body>
</html>
