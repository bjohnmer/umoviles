<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Unidades Motorizadas Retenidas</title>
  
  <?php if (!empty($css_files)): ?>
  <?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
  <?php endforeach; ?>
  <?php endif ?>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/css/jumbotron.css" rel="stylesheet">
    
    <link href="<?=base_url()?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/personalizados.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>assets/js/html5shiv.js"></script>
      <script src="<?=base_url()?>assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="<?=base_url()?>sesion" class="navbar-brand">Unidades Motorizadas Retenidas</a>
          </div>
          <div class="navbar-collapse collapse" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li><a href="<?=base_url()?>sesion/unidades">Unidades</a></li>
              <li><a href="<?=base_url()?>sesion/conductores">Conductores</a></li>
              <li><a href="<?=base_url()?>sesion/retenciones">Retenciones</a></li>
              <li><a href="<?=base_url()?>sesion/estadisticas">Estadísticas</a></li>
              <?php if ($this->session->userdata("tipo") == "Administrador"): ?>
                <li><a href="<?=base_url()?>sesion/usuarios">Usuarios</a></li>
              <?php endif ?>  
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><?=$this->session->userdata("nombre")?></a></li>
            <a href="<?=base_url()?>sesion/cerrar_sesion" class="btn btn-danger navbar-btn">Cerrar sesión</a>
          </ul>
          </div><!--/.navbar-collapse -->
        </div>
      </div>
  
      <?php if (!empty($mensaje)): ?>
        <div class="alert alert-<?=$mensaje['tipo']?> alert-dismissable" style="margin-bottom:0;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?=$mensaje['mensaje']?>
        </div>
      <?php endif ?>

