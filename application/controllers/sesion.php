<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sesion extends CI_Controller {

	private $sources;

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
    if (!$this->session->userdata("logged_in"))
    {
      redirect('/');
    }

		$this->sources = $this->constantes->assets();
		
		$this->load->library("grocery_CRUD");


	}
		
	public function index($data = null)
	{
		$this->load->view('includes/cabeceraadministrador');
		$this->load->view('backend/vsesion',$data);
		$this->load->view('includes/pieadministrador');
	}

	public function unidades()
	{
		$unidades = new grocery_CRUD();
		$unidades->set_table("unidades");
		
		$unidades->set_subject("Unidad");

		$unidades->columns("marca","modelo","anio","smotor","scarroceria","color","placa");

		// $unidades->display_as('anio', 'Año');
		$unidades->display_as('smotor', 'Serial de Motor');
		$unidades->display_as('scarroceria', 'Serial de Carrocería');

		$unidades->set_rules("marca","Marca","required|max_length[45]");
		$unidades->set_rules("modelo","Modelo","required|max_length[45]");
		$unidades->set_rules("anio","Año","required|numeric|max_length[4]|min_length[4]");
		$unidades->set_rules("smotor","Serial de Motor","required|max_length[45]");
		$unidades->set_rules("scarroceria","Serial de Carrocería","required|max_length[45]");
		$unidades->set_rules("color","Color","required|max_length[45]");
		$unidades->set_rules("placa","Placa","max_length[45]");
		
		$unidades->add_action('Retenciones', '-', "sesion/verRUnidades",'');
				
		if ($this->session->userdata("tipo") == "Supervisor"):
			$unidades->unset_delete();
			$unidades->unset_edit();
		endif;

		$output = $unidades->render();
		
		$this->load->view('includes/cabeceraadministrador',$output);
		$this->load->view('unidades/vunidades');
		$this->load->view('includes/pieadministrador');
		
	}
	
	public function conductores()
	{
		
		$conductores = new grocery_CRUD();
		$conductores->set_table("conductores");
		
		$conductores->set_subject("Conductor");

		$conductores->columns("cedula","nombres","apellidos","telf");

		// $conductores->display_as('anio', 'Año');
		$conductores->display_as('cedula', 'Cédula');
		$conductores->display_as('telf', 'Teléfonos');
		$conductores->display_as('direccion', 'Dirección');

		$conductores->set_rules("cedula","Cédula","required|max_length[12]");
		$conductores->set_rules("nombres","Nombres","required|max_length[75]");
		$conductores->set_rules("apellidos","Apellidos","required|max_length[75]");
		$conductores->set_rules("direccion","Dirección","required|min_length[8]");
		$conductores->set_rules("telf","Teléfonos","required|max_length[50]");
		$conductores->unset_texteditor("direccion");
		
		$conductores->add_action('Retenciones', '-', "sesion/verRConductores",'');

		if ($this->session->userdata("tipo") == "Supervisor"):
			$conductores->unset_delete();
			$conductores->unset_edit();
		endif;

		$output = $conductores->render();
		
		$this->load->view('includes/cabeceraadministrador',$output);
		$this->load->view('conductores/vconductores');
		$this->load->view('includes/pieadministrador');
		
	}

	function verRConductores()
	{

		$retenciones = new grocery_CRUD();
		$retenciones->set_table("retenciones");
		
		$retenciones->set_subject("Retención");

		$retenciones->columns("fecha","unidades_id","conductores_id","condiciones","estatus");
		$retenciones->field_type("fecha","hidden",date("Y-m-d"));
		$retenciones->set_field_upload("imagen","assets/uploads/imagenes");
		
		$retenciones->where("conductores_id",$this->uri->segment(3));

		$retenciones->set_relation("unidades_id","unidades","{scarroceria} - {placa} {marca} {modelo}");
		$retenciones->set_relation("conductores_id","conductores","{cedula} - {nombres} {apellidos}");

		// $retenciones->display_as("fecha","fecha");
		$retenciones->display_as("unidades_id","Unidad");
		$retenciones->display_as("conductores_id","Conductor");
		// $retenciones->display_as("condiciones","condiciones");
		$retenciones->display_as("cretencion","Causa de Retención");
		$retenciones->display_as("dretencion","Detalle de Retención");
		// $retenciones->display_as("estatus","estatus");
		$retenciones->display_as("imagen","Imágen");
		// $retenciones->display_as("licencia","licencia");
		$retenciones->display_as("cmedica","Carta Médica");
		$retenciones->display_as("rcivil","Seguro RCV");
		$retenciones->display_as("cmotorizado","Carnet de Motorizado");
		$retenciones->display_as("corigen","Certificado de Orígen");


		// $retenciones->set_rules("fecha","fecha","required|");
		$retenciones->set_rules("unidades_id","Unidad","required");
		$retenciones->set_rules("conductores_id","Conductor","required");
		$retenciones->set_rules("condiciones","condiciones","required|min_length[4]");
		$retenciones->set_rules("cretencion","Causa de Retención","required");
		$retenciones->set_rules("dretencion","Detalle de Retención","min_length[8]");
		$retenciones->set_rules("imagen","Imágen","required");
		$retenciones->set_rules("licencia","licencia","required");
		$retenciones->set_rules("cmedica","Carta Médica","required");
		$retenciones->set_rules("rcivil","Seguro RCV","required");
		$retenciones->set_rules("cmotorizado","Carnet de Motorizado","required");
		$retenciones->set_rules("corigen","Certificado de Orígen","required");
		$retenciones->set_rules("estatus","estatus","required");
		
		$retenciones->unset_texteditor("condiciones");
		$retenciones->unset_texteditor("dretencion");

		$retenciones->unset_add();
		if ($this->session->userdata("tipo") == "Supervisor"):
			$retenciones->unset_delete();
			$retenciones->unset_edit();
		endif;

		$output = $retenciones->render();
		
		$this->load->view('includes/cabeceraadministrador',$output);
		$this->load->view('retenciones/vretenciones');
		$this->load->view('includes/pieadministrador');
		
	}


	function verRUnidades()
	{

		$retenciones = new grocery_CRUD();
		$retenciones->set_table("retenciones");
		
		$retenciones->set_subject("Retención");

		$retenciones->columns("fecha","unidades_id","conductores_id","condiciones","estatus");
		$retenciones->field_type("fecha","hidden",date("Y-m-d"));
		$retenciones->set_field_upload("imagen","assets/uploads/imagenes");
		
		$retenciones->where("unidades_id",$this->uri->segment(3));

		$retenciones->set_relation("unidades_id","unidades","{scarroceria} - {placa} {marca} {modelo}");
		$retenciones->set_relation("conductores_id","conductores","{cedula} - {nombres} {apellidos}");

		// $retenciones->display_as("fecha","fecha");
		$retenciones->display_as("unidades_id","Unidad");
		$retenciones->display_as("conductores_id","Conductor");
		// $retenciones->display_as("condiciones","condiciones");
		$retenciones->display_as("cretencion","Causa de Retención");
		$retenciones->display_as("dretencion","Detalle de Retención");
		// $retenciones->display_as("estatus","estatus");
		$retenciones->display_as("imagen","Imágen");
		// $retenciones->display_as("licencia","licencia");
		$retenciones->display_as("cmedica","Carta Médica");
		$retenciones->display_as("rcivil","Seguro RCV");
		$retenciones->display_as("cmotorizado","Carnet de Motorizado");
		$retenciones->display_as("corigen","Certificado de Orígen");


		// $retenciones->set_rules("fecha","fecha","required|");
		$retenciones->set_rules("unidades_id","Unidad","required");
		$retenciones->set_rules("conductores_id","Conductor","required");
		$retenciones->set_rules("condiciones","condiciones","required|min_length[4]");
		$retenciones->set_rules("cretencion","Causa de Retención","required");
		$retenciones->set_rules("dretencion","Detalle de Retención","min_length[8]");
		$retenciones->set_rules("imagen","Imágen","required");
		$retenciones->set_rules("licencia","licencia","required");
		$retenciones->set_rules("cmedica","Carta Médica","required");
		$retenciones->set_rules("rcivil","Seguro RCV","required");
		$retenciones->set_rules("cmotorizado","Carnet de Motorizado","required");
		$retenciones->set_rules("corigen","Certificado de Orígen","required");
		$retenciones->set_rules("estatus","estatus","required");
		
		$retenciones->unset_texteditor("condiciones");
		$retenciones->unset_texteditor("dretencion");

		$retenciones->unset_add();
		if ($this->session->userdata("tipo") == "Supervisor"):
			$retenciones->unset_delete();
			$retenciones->unset_edit();
		endif;

		$output = $retenciones->render();
		
		$this->load->view('includes/cabeceraadministrador',$output);
		$this->load->view('retenciones/vretenciones');
		$this->load->view('includes/pieadministrador');
		
	}

	public function retenciones()
	{
		
		$retenciones = new grocery_CRUD();
		$retenciones->set_table("retenciones");
		
		$retenciones->set_subject("Retención");

		$retenciones->columns("fecha","unidades_id","conductores_id","condiciones","estatus");
		$retenciones->field_type("fecha","hidden",date("Y-m-d"));
		$retenciones->set_field_upload("imagen","assets/uploads/imagenes");
	
		$retenciones->set_relation("unidades_id","unidades","{scarroceria} - {placa} {marca} {modelo}");
		$retenciones->set_relation("conductores_id","conductores","{cedula} - {nombres} {apellidos}");

		// $retenciones->display_as("fecha","fecha");
		$retenciones->display_as("unidades_id","Unidad");
		$retenciones->display_as("conductores_id","Conductor");
		// $retenciones->display_as("condiciones","condiciones");
		$retenciones->display_as("cretencion","Causa de Retención");
		$retenciones->display_as("dretencion","Detalle de Retención");
		// $retenciones->display_as("estatus","estatus");
		$retenciones->display_as("imagen","Imágen");
		// $retenciones->display_as("licencia","licencia");
		$retenciones->display_as("cmedica","Carta Médica");
		$retenciones->display_as("rcivil","Seguro RCV");
		$retenciones->display_as("cmotorizado","Carnet de Motorizado");
		$retenciones->display_as("corigen","Certificado de Orígen");


		// $retenciones->set_rules("fecha","fecha","required|");
		$retenciones->set_rules("unidades_id","Unidad","required");
		$retenciones->set_rules("conductores_id","Conductor","required");
		$retenciones->set_rules("condiciones","condiciones","required|min_length[4]");
		$retenciones->set_rules("cretencion","Causa de Retención","required");
		$retenciones->set_rules("dretencion","Detalle de Retención","min_length[8]");
		$retenciones->set_rules("imagen","Imágen","required");
		$retenciones->set_rules("licencia","licencia","required");
		$retenciones->set_rules("cmedica","Carta Médica","required");
		$retenciones->set_rules("rcivil","Seguro RCV","required");
		$retenciones->set_rules("cmotorizado","Carnet de Motorizado","required");
		$retenciones->set_rules("corigen","Certificado de Orígen","required");
		$retenciones->set_rules("estatus","estatus","required");
		
		$retenciones->unset_texteditor("condiciones");
		$retenciones->unset_texteditor("dretencion");

		if ($this->session->userdata("tipo") == "Supervisor"):
			$retenciones->unset_delete();
			$retenciones->unset_edit();
		endif;

		$output = $retenciones->render();
		
		$this->load->view('includes/cabeceraadministrador',$output);
		$this->load->view('retenciones/vretenciones');
		$this->load->view('includes/pieadministrador');
		
	}
	
	public function estadisticas()
	{
		echo "ESTADISTICAS";
	}

	public function usuarios()
  {
    
    $usuarios = new grocery_CRUD();
    $usuarios->set_table('usuarios');
    $usuarios->set_subject('Usuario');
    
    $usuarios->display_as('nombre', 'Nombre');
    $usuarios->display_as('login', 'Login');
    $usuarios->display_as('clave', 'Clave');
    $usuarios->display_as('conf', 'Confirmación de Clave');
    $usuarios->display_as('tipo', 'Tipo de Usuario');
    
    $usuarios->columns("nombre","login","tipo");
    $usuarios->field_type('clave', 'password');
    $usuarios->field_type('conf', 'password');
    
    $usuarios->set_rules('nombre', 'Nombre del Usuario',"required|alpha_space|max_length[45]|min_length[2]");
    $usuarios->set_rules('login', 'Login',"required|alpha|max_length[20]|min_length[4]");
    $usuarios->set_rules('clave', 'Clave',"max_length[20]|min_length[3]|matches[conf]");
    $usuarios->set_rules('conf', 'Confirmación de Clave',"max_length[20]|min_length[3]|matches[clave]");
    $usuarios->set_rules('tipo', 'Tipo de Usuario',"required");

    $usuarios->callback_before_insert(array($this,'encrypt_clave_callback'));
    $usuarios->callback_before_update(array($this,'encrypt_confirmar_callback'));

    $usuarios->callback_edit_field('clave',array($this,'vaciar_campo_clave'));
    $usuarios->callback_edit_field('conf',array($this,'vaciar_campo_confirmar'));

    $output = $usuarios->render();
   
   	$this->load->view('includes/cabeceraadministrador',$output);
   	$this->load->view('usuarios/vusuarios');
   	$this->load->view('includes/pieadministrador');
  
  }

  function encrypt_clave_callback($post_array, $primary_key = null)
  {
      
      $post_array['clave'] = sha1($post_array['clave']);
      $post_array['conf'] = sha1($post_array['conf']);

      return $post_array;
  }

  function encrypt_confirmar_callback($post_array, $primary_key = null)
  {
      
      if (!empty($post_array['clave']))
      {
          
          $post_array['clave'] = sha1($post_array['clave']);
          $post_array['conf'] = sha1($post_array['conf']);
      
      }
      else
      {
      
          unset($post_array['clave']);
          unset($post_array['conf']);
      
      }
      
      return $post_array;
  
  }

  function vaciar_campo_clave($post_array, $primary_key = null)
  {
      
      return '<input type="password" maxlength="50" name="clave" id="field-clave">';
  
  }

  function vaciar_campo_confirmar($post_array, $primary_key = null)
  {
      
      return '<input type="password" maxlength="50" name="conf" id="field-conf">';
  
  }


	public function cerrar_sesion()
	{	
	  $this->session->set_userdata("logged_in", FALSE);
	  $this->session->sess_destroy();
	  redirect('/');
	}
}