<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicial extends CI_Controller {

	private $sources;

	function __construct()
	{

		parent::__construct();

		$this->sources = $this->constantes->assets();
		$this->load->model("mautenticacion");
    $this->load->library("form_validation");
	
	}
		
	public function index($data = null)
	{
		$this->load->view('frontend/vinicial',$data);
	}

	public function autenticacion()
	{
		$this->form_validation->set_rules("login", "Usuario", "required|trim|max_length[20]");
		$this->form_validation->set_rules("clave", "Clave", "required|trim|max_length[20]");
		if ($this->form_validation->run() == FALSE) 
		{
		  $this->index();
		} 
		else 
		{
		  $clave = sha1($this->input->post("clave"));
		  $data = $this->mautenticacion->validar($this->input->post("login"), $clave);
		  if ($data) 
		  {
		   	$this->load->library("session");
		    $this->session->set_userdata("logged_in" , TRUE);
		    $this->session->set_userdata("id",$data->id);
		    $this->session->set_userdata("nombre",$data->nombre);
		    $this->session->set_userdata("login",$data->login);
		    $this->session->set_userdata("tipo",$data->tipo);
		    redirect('sesion/');
		  } 
		  else  
		  {
		    $data['mensaje']['tipo'] = "danger";
		    $data['mensaje']['mensaje'] = "Nombre de usuario o clave inválida";
		    $this->index($data);
		  }
		}

	}
}