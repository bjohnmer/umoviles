-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-09-2013 a las 01:23:05
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `umoviles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conductores`
--

CREATE TABLE IF NOT EXISTS `conductores` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla de Conductores',
  `cedula` varchar(12) NOT NULL COMMENT 'Cédula o Documento de identidad del conductor',
  `nombres` varchar(75) NOT NULL COMMENT 'Primer y segundo nombre del conductor',
  `apellidos` varchar(75) NOT NULL COMMENT 'Primer y segundo apellido del conductor',
  `direccion` text NOT NULL COMMENT 'Dirección de habitación del conductor',
  `telf` varchar(50) DEFAULT NULL COMMENT 'Teléfono del conductor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de los conductores de las unidades retenidas' AUTO_INCREMENT=54 ;

--
-- Volcado de datos para la tabla `conductores`
--

INSERT INTO `conductores` (`id`, `cedula`, `nombres`, `apellidos`, `direccion`, `telf`) VALUES
(1, '23.776.683', 'EDIXON', 'MONTILL', 'MONAY, CALLE PRINCIPAL', 'NO POSEE'),
(2, '19.610.983', 'HERNAN', 'MARIN ', 'MONAY PQUIA LA PAZ', 'NO POSEE'),
(3, '25.201.482', 'JOHAN', 'PEREZ GIL', 'MONAY PARROQUIA LA PAZ', 'NO POSEE'),
(4, '14800610', 'OVIDIO', 'PERES', 'MONAY CALLE EL EMPEDRADO', 'NO POSEE'),
(5, '17.547.964', 'HELFRYS', 'ACOSTA', 'MONAY PARROQUIA LA PAZ', 'NO POSEE'),
(6, '20.706.465', 'FRANKLIN', 'TAPIA', 'MONAY PARROQUIA LA PAZ', 'NO PORTA'),
(7, '26.591.702', 'YILBER', 'ARTIGAS', 'MONAY PQUIA LA PAZ ', 'NO POSEE'),
(8, '23.775605', 'EDWIN', 'GONZALEZ', 'MONAY PQUIA LA PAZ', 'NO POSEE'),
(9, '25.201.482', 'ANGEL', 'ESIS', 'MONAY PAQUIA LA PAZ', 'NO POSEE'),
(10, '20.133.799', 'GABRIEL ', 'PERDOMO', 'MONAR CALLE SAN RAFAEL', 'NO POSEE'),
(14, '19.609.430', 'ALBERTO ', 'DURAN', 'LA RECTA DE MONAY', 'NO POSEE'),
(15, '21.103.380', 'MANUEL', 'PRADO', 'LA RECTA DE MONAY', 'NO POSEE'),
(16, '13.377.268', 'ALBERTO ANTONIO', 'RANGEL DURAN', 'LA RECTA DE MONAY', 'NO TIENE'),
(17, '25.767.892', 'LUIS MANUEL', 'BRICEÑO', 'SANTA LUCIA', 'NO TIENE'),
(18, '25.562.809', 'JOSE CAMILO', 'RODRIGUEZ', 'MONAY CALLE SAN RAFAEL', 'NO TIENE'),
(19, '18.035.707', 'JAVIER', 'GUDIÑO', 'CALLE LIBRE MONAY', 'NO TIENE'),
(20, '19.812.797', 'JOSÉ LUIS', 'BLANCO ARAUJO', 'MONAY AV PRINCIPAL', 'NO POSEE'),
(21, '18.250.338', 'JOSÉ WILFREDO', 'TERAN TORRES', 'MONAY CALLE PRINCIPAL', 'NO POSEE'),
(22, '16.955.808', 'LUIS JOHAN', 'RAMOS PEREZ', 'LA RECTA DE MONAY', 'NO TIENE'),
(23, '19.148.146', 'ROBERTO ', 'RUZA', 'EL LIMON CALLE PRINCIPAL', 'NO TIENE'),
(24, '24.618.979', 'YORBIS ', 'ROMAN', 'MONAY CALLE EL LICEO', 'NO TIENE'),
(25, '25.173.378', 'LUIS ALBERTO', 'MARQUEZ', 'LA RECTA DE MONAY', 'NO TIENE'),
(26, '17.598.024', 'BENITO', 'ROJAS', 'MONAY AV PRINCIPAL', 'NO TIENE'),
(27, '20.604.220', 'JOKLIN', 'ALVARADO', 'CALLE LAS ACACIAS', 'NO POSEE'),
(28, '24.138.272', 'ANTONIO', 'SALAS', 'EL LIMON MONAY', 'NO TIENE'),
(29, '26.114.750', 'JORBI ', 'BOLIVAR', 'MONAY CALLE SAN RAFAEL', 'NO TIENE'),
(30, '17.596.520', 'SAMUEL', 'TORRES', 'MONAY URB LA PAZ', 'NO TIENE'),
(31, '24.786.799', 'ANGELO', 'LOZADA', 'URB. LA PAZ DE MONAY', 'NO TIENE'),
(32, '23.742.074', 'JEHONDRI', 'SOTO', 'SECTOR DON ALFREDO MONAY', 'NO TIENE'),
(33, '27.752637', 'FRANK', 'DELGADO', 'SECTOR SAN BENITO MONAY', 'NO TIENE'),
(34, '27.752.637', 'FRANK', 'DELGADO', 'LA RECTA DE MONAY ', 'NO TIENE'),
(35, '21.105.507', 'ENRIQUE', 'MONTILLA', 'SANTA LUCIA DE MONAY', 'NO TIENE'),
(36, '21.009.959', 'JHONATAN', 'AGUILERA', 'SECTOR EL PREGRESO', 'NO TIENE'),
(37, '20.706.532', 'KELVIN', 'GRATEROL', 'SECTOR MARACAIBITO DE MONAY', 'NO TIENE'),
(38, '17.345.877', 'KLEIVER', 'GRATEROL', 'LA GARITA II DE MONAY', 'NO POSEE'),
(39, '11.663.005', 'RICHARD', 'TORRES', 'LA RECTA DE MONAY', 'NO POSEE'),
(40, '17.037.494', 'JESUS', 'VILLEGAS', 'CAMPO ESTRELLA DE MONAY', 'NO TIENE'),
(41, '25.006-375', 'DANIEL', 'MATERANO', 'EL MACOYAL DE MONAY', 'NO TIENE'),
(42, '10.313.924', 'MISAEL', 'AZUAJE', 'SECTOR LA VEGA DE MONAY', 'NO TIENE'),
(44, '25.919.114', 'DANIEL', 'BRICEÑO', 'SECTOR LA VEGA DE MONAY', 'NO POSEE'),
(45, '27.889006', 'YOENDER', 'DURAN', 'MONAY PAQUIA LA PAZ', 'NO TIENE'),
(47, '27.889.006', 'YOENDER', 'DURAN', 'MONAY CALLE LAS ACACIAS', 'NO POSEE'),
(48, '16.465.647', 'DANNY JOSÉ ', 'CACERES', 'MONAY CALLE LAS ACACIAS', 'NO TIENE'),
(49, '14.780.039', 'REMIGIO ALEXIS', 'GONZALEZ', 'MONAY CALLE EL CEMENTERIO', 'NO TIENE'),
(50, '25.882.758', 'LUIS', 'VILLEGAS', 'CALLE EL CASABE MONAY', 'NO POSEE'),
(51, '21.209.136', 'JOSÉ DANIEL', 'VALERA', 'MONAY CALLE LA PAZ ', 'NO POSEE'),
(52, '25.566.936', 'JOSE ', 'RODRIGUEZ', 'MONAY CALLE EL LICEO ', 'NO POSEE'),
(53, '26.757.809', 'PEDRO', 'CASTELLANO', 'MONAY SECTOR MARACAIBITO', 'NO POSEE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones`
--

CREATE TABLE IF NOT EXISTS `retenciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Campo clave de la tabla de retenciones',
  `fecha` date NOT NULL COMMENT 'Fecha de la retención',
  `unidades_id` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de Unidades',
  `conductores_id` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de Conductores',
  `condiciones` text NOT NULL COMMENT 'Condiciones de la unidad retenida',
  `cretencion` enum('293','Casco','Documentación','Otro') NOT NULL DEFAULT '293' COMMENT 'Causa de la retención',
  `dretencion` text NOT NULL COMMENT 'Detalle de la retención',
  `estatus` enum('Devuelta[293]','Tránsito','Fiscalía') NOT NULL DEFAULT 'Devuelta[293]' COMMENT 'Estatus de la unidad',
  `imagen` varchar(255) DEFAULT NULL COMMENT 'Foto de la unidad retenida',
  `licencia` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación de Licencia de Conducir',
  `cmedica` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación de la Carta Médica',
  `rcivil` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación del Seguro de Responsabilidad Civil',
  `cmotorizado` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y validación del Carnet de Motorizado',
  `corigen` enum('Si','No') DEFAULT 'No' COMMENT 'Porte y Validación del Certificado de Orígen',
  PRIMARY KEY (`id`),
  KEY `fk_retenciones_unidades` (`unidades_id`),
  KEY `fk_retenciones_conductores1` (`conductores_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Histórico de retetenciones de las unidades móviles' AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `retenciones`
--

INSERT INTO `retenciones` (`id`, `fecha`, `unidades_id`, `conductores_id`, `condiciones`, `cretencion`, `dretencion`, `estatus`, `imagen`, `licencia`, `cmedica`, `rcivil`, `cmotorizado`, `corigen`) VALUES
(1, '2013-09-26', 2, 1, 'POSEE RETROVISOR, BATERIAS, NEUMATICOS NUEVOS', 'Documentación', 'NI CARTA MEDICA, LICENCIA, SEGURO DE RESPONSABILIDAD CIVIL', 'Tránsito', 'dfb4f-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(2, '2013-09-26', 3, 2, 'RETROVISORES, BATERIA, TANQUE, NEUMATICOS EN BUENAS COND', 'Casco', 'NO PORTA CASCO, NI CARTA MEDICA', 'Tránsito', '14d1b-img-20130620-00867.jpg', 'Si', 'Si', 'No', 'No', 'No'),
(3, '2013-09-26', 4, 3, 'NEUMATICOS EN BUEN ESTADO, RETROVISORES, BATERIA', 'Documentación', 'NO PORTA NINGUN TIPO DE DOCUMENTACION, NI CASCO DE SEGURIDAD', 'Tránsito', 'cb38a-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(4, '2013-09-26', 5, 4, 'RETROVISORES, NEUMATICOS, BATERIA', 'Documentación', 'NO PORTABA DOCUMENTACION NI CASCO DE SEGURIDAD', 'Tránsito', '3668f-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(5, '2013-09-26', 7, 5, 'NEUMATICOS BUENOS, RETROVISORES BATERIA', 'Casco', 'NO PORTA NINGUN TIPO DE DOCUMENTACIÓN ', 'Tránsito', '1ebc4-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(6, '2013-09-26', 8, 6, 'DOS NEUMATICOS BUENOS RETROVISORES', 'Casco', 'NO PORTA CASCO DE SEGURIDAD', 'Tránsito', '30051-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(7, '2013-09-26', 9, 7, 'BUENAS CONDICIONES ', 'Documentación', 'NO POSEE NINGUN TIPO DE DOCUEMENTACION', 'Tránsito', '95100-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(8, '2013-09-26', 10, 8, 'BUENAS CONDICIONES', 'Documentación', 'NO POSEE LICENCIA', 'Tránsito', '5bd8a-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(9, '2013-09-26', 11, 14, 'EN BUENAS CONDICIONES', 'Casco', 'NO PORTA CASCO DE SEGURIDAD', 'Tránsito', '1cc09-img-20121209-00124.jpg', 'Si', 'No', 'No', 'No', 'No'),
(10, '2013-09-26', 12, 15, 'BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA CASCO DE SEGURIDAD', 'Tránsito', 'b4275-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(11, '2013-09-26', 13, 27, 'EN BUENAS CONDICIONES CON TODOS SUS ACCESORIOS', 'Documentación', 'AL MOMENTO DE LA RETENCION NO PORTABA DOCUMENTOS', 'Fiscalía', '7787f-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'Si'),
(12, '2013-09-26', 14, 16, 'CON TODOS SUS ACCESORIOS EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA DOCUMENTOS AL MOMENTO DE LA RETENCION', 'Fiscalía', '7f35c-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(13, '2013-09-26', 15, 17, 'LOS ACCESORIOS SE ENCUENTRAN EN BUEN ESTADO', 'Otro', 'SE ENCONTRABA SOLICITADA POR EL CICPC', 'Fiscalía', 'e2238-img-20130620-00867.jpg', 'No', 'Si', 'No', 'No', 'No'),
(14, '2013-09-26', 16, 18, 'NO POSEE RETROVISORES Y LAS LUCES DELANTERAS DE CRUCE ESTAN PARTIDAS', '293', 'SE ENCONTRABA FUERA DE LA HORAS Y DOS DEL MISMO SEXO', 'Devuelta[293]', '785dc-img-20121209-00124.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(15, '2013-09-26', 17, 19, 'EN BUENAS CONDICIONES Y TIENE BATERIA', '293', 'TRES PERSONAS DEL MISMO SEXO EN LA UNIDAD', 'Devuelta[293]', '2dfec-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(16, '2013-09-26', 18, 21, 'EN BUENAS CONDICIONES Y CON TODOS SUS ACCESORIOS', 'Documentación', 'NO PORTAR NINGUN TIPO DE DOCUMENTACION', 'Tránsito', '5f697-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(17, '2013-09-26', 19, 21, 'CON TODOS SUS ACCESORIOS Y EN BUENAS CONDICIONES ', 'Otro', 'EN ESTADO DE ABANDONO', 'Fiscalía', '279e3-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(18, '2013-09-27', 20, 22, 'EN BUENAS CONDICIONES', 'Otro', 'POR ENCONTRARSE EN ESTADO DE EBRIEDAD AL MOMENTO DE CONDUCIR LA UNIDAD', 'Tránsito', '36c14-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(19, '2013-09-27', 21, 23, 'LE FALTA LAS LUCES DEL CRUCE TRASERO', 'Casco', 'AL MOMENTO DE LA RETENCION NO PORTABA CASCO Y POR RESISTIRSE A LA COMISION', 'Tránsito', '52580-img-20121209-00124.jpg', 'Si', 'Si', 'No', 'No', 'No'),
(20, '2013-09-27', 22, 24, 'EN BUENAS CONDICIONES ', 'Casco', 'NO PORTA CASCO DE SEGURIDAD', 'Tránsito', 'd1ddf-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'No', 'No'),
(21, '2013-09-27', 23, 24, 'NO TIENE LUCES TRASERAS DE CRUCE', 'Casco', 'NO PORTABA CASCO DE SEGURIDAD', 'Tránsito', 'd8a9c-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'No', 'No'),
(22, '2013-09-27', 24, 26, 'NO POSEE RETROVISORES', 'Casco', 'NO PORTA CASCO DE SEGURIDAD', 'Tránsito', 'cea57-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'No', 'No'),
(23, '2013-09-27', 25, 28, 'EN BUENAS CONDICIONES', 'Documentación', 'NO PORTA NINGUN TIPO DE DOCUMENTACION', 'Tránsito', '2dc2f-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(24, '2013-09-27', 26, 29, 'EN BUENAS CONDICIONES Y CON BATERIA', 'Documentación', 'NO PORTABA CASCO NI DOCUMENTOS DE PROPIEDAD DE LA MOTO', 'Tránsito', '477c8-img-20130620-00867.jpg', 'Si', 'Si', 'No', 'No', 'No'),
(25, '2013-09-27', 27, 30, 'EN BUEN ESTADO, TIENE RETROVISORES Y BATERIA', 'Documentación', 'NO PORTABA NINGUN TIPO DE DOCUMENTOS', 'Tránsito', '9899c-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(26, '2013-09-27', 25, 5, 'SIN RETROVISOR, TIENE BATERIA ', '293', 'DOS DEL MISMO SEXO', 'Devuelta[293]', '0f598-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(27, '2013-09-27', 29, 32, 'EN BUENAS CONDICIONES', 'Documentación', 'NO PORTA CASCO DE SEGURIDAD', 'Tránsito', '1a9a6-img-20130620-00867.jpg', 'No', 'No', 'No', 'No', 'No'),
(28, '2013-09-27', 30, 34, 'EN BUENAS CONDICIONES', 'Documentación', 'NO PORTABA NINGUN TIPO DE DOCUMENTACION', 'Tránsito', 'ab824-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(29, '2013-09-27', 40, 36, 'EN BUENAS CONDICIONES', 'Documentación', 'NO PORTABA NINGÚN TIPO DE DOCUMENTACIÓN ', 'Tránsito', 'b0cdf-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(30, '2013-09-27', 32, 35, 'EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABAB LICENCIAS Y SE ENCONTRABA EN ESTADO DE EMBRIAGUEZ', 'Tránsito', 'b7878-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(31, '2013-09-27', 33, 37, 'NO TIENE RETROVISOR DERECHO', 'Documentación', 'NO PORTABA LICENCIA NI CARTA MEDICA', 'Tránsito', '8e426-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(32, '2013-09-27', 34, 38, 'EN BUENAS CONDICIONES', 'Documentación', 'NO POSEE DOCUMENTOS ', 'Tránsito', 'ee528-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(33, '2013-09-27', 35, 39, 'NO TIENE LUCES DE CRUCE TRASERAS ', 'Documentación', 'NO POSEE DOSCUMENTOS', 'Tránsito', '3b40b-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(34, '2013-09-27', 36, 40, 'NO POSEE LUCES DE CRUCE', 'Documentación', 'NO PORTABA CASCO AL MOMENTO DE LA RETENCION', 'Tránsito', '86b6f-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(35, '2013-09-27', 37, 41, 'EN BUENAS CONDICIONES CON TODOS LOS ACCESORIOS', 'Documentación', 'EN ESTADO DE EBRIEDAD AL MOMENTO DE LA RETENCION', 'Tránsito', '623f5-img-20121209-00124.jpg', 'No', 'Si', 'No', 'No', 'No'),
(36, '2013-09-27', 38, 42, 'EN BUENAS CONDICIONES ', '293', 'POR ESTAR FUERA DE LA HORA ESTABLECIDA', 'Devuelta[293]', 'd7f99-img-20130620-00867.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(37, '2013-09-27', 39, 44, 'EN BUENAS CONDICIONES', '293', 'DOS PERSONAS DEL MISMO SEXO', 'Devuelta[293]', '9f74e-img-20121209-00124.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(38, '2013-09-27', 41, 45, 'EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA CASCO AL MOMENTO DE LA RETENCION', 'Tránsito', '205af-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(39, '2013-09-27', 42, 48, 'EN BUENAS CONDICIONES', 'Documentación', 'NO PORTABA CASCO DE SEGURIDAD', 'Tránsito', 'c6cde-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(40, '2013-09-27', 43, 49, 'EN BUENAS CONDICIONES ', '293', 'CIRCULAR EN EL HORARIO NO PERMITIDO', 'Devuelta[293]', 'ec0c7-img-20121209-00124.jpg', 'Si', 'Si', 'Si', 'Si', 'Si'),
(41, '2013-09-27', 44, 50, 'LE FALTA EL RETROVISOR DEL LADO DERECHO', 'Casco', 'NO PORTABA NINGUN TIPO DE DOCUMENTACIÓN ', 'Tránsito', '7d574-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(42, '2013-09-27', 45, 51, 'EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA CASCO AL MOMENTO DE LA RETENCION', 'Tránsito', '91c39-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(43, '2013-09-27', 46, 52, 'EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA CASCO AL MOMENTO DE LA RETENCION', 'Tránsito', '646ad-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No'),
(44, '2013-09-27', 47, 53, 'EN BUENAS CONDICIONES ', 'Documentación', 'NO PORTABA CASCO DE SEGURIDAD AL MOMENTO DE LA RETENCION ', 'Tránsito', '79eac-img-20121209-00124.jpg', 'No', 'No', 'No', 'No', 'No');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE IF NOT EXISTS `unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Campo clave de la tabla de unidades',
  `marca` varchar(45) NOT NULL COMMENT 'Marca de la moto',
  `modelo` varchar(45) NOT NULL COMMENT 'Modelo de la moto',
  `anio` int(11) NOT NULL COMMENT 'Año de la moto',
  `smotor` varchar(45) NOT NULL COMMENT 'Serial de motor',
  `scarroceria` varchar(45) NOT NULL COMMENT 'Serial de carrocería',
  `color` varchar(45) NOT NULL COMMENT 'Color de la moto',
  `placa` varchar(45) DEFAULT NULL COMMENT 'Placa de la moto',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de nidades retenidasu' AUTO_INCREMENT=48 ;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id`, `marca`, `modelo`, `anio`, `smotor`, `scarroceria`, `color`, `placa`) VALUES
(2, 'ava', 'br150', 2007, 'no posee', 'LBRSPKB0079011904', 'GRIS', 'MCY-717'),
(3, 'BERA', 'BR 150', 2012, '162FMJB9100687', '821LMBCA8CD200799', 'ROJO', 'AB3F79K'),
(4, 'EMPIRE', 'HORSE 150', 2012, 'KW162FMJ1415465', '812K3AC17CM058365', 'ROJO', 'AA9F94W'),
(5, 'EMPIRE', 'HORSE II', 2011, 'KW162FMJ1415465', '812K3UC11BM017455', 'AZUL', 'AA8C17C'),
(6, 'MD', 'HAOJIN 150', 2004, 'HJ162FMJ2695926', '813RSMECA4CV00319', 'ROJO', 'AA2N50J'),
(7, 'EMPIRE', 'TX 200', 2012, 'KW164FML0423292', '8122K1M27CM001626', 'AZUL', 'AA2X81T'),
(8, 'BERA', 'BR 150', 2012, 'SK162FMJ1200388484', '8211MBCA7CD046318', 'PLATA', 'NO PORTA'),
(9, 'BERA', 'BR 150', 2008, '163FML85012418', 'LP8PCMA0880B2205', 'ROJO', 'AA6J70D'),
(10, 'BERA', 'BR 150', 2013, 'SK162FMJ1200378753', '8211MPCAXCD31117', 'ROJO', 'AC4R12K'),
(11, 'BERA', 'BR 200', 2008, '163FML85012218', 'LP6PCMA0880B0509', 'AZUL', 'NO PORTA'),
(12, 'BERA', 'BR 200', 2011, 'NO TIENE', '821MZ4C38BD004228', 'ROJO', 'AD6F22D'),
(13, 'EMPIRE', 'HORSE', 2013, 'KW162FMJ2677868', '8123A1K14DM019424', 'NEGRO', 'AK6E08A'),
(14, 'BERA', 'BR 150', 2009, '162FMJ94406642', 'L3YPCKLC89A406111', 'ROJO', 'AC4H52D'),
(15, 'BERA', 'BR 150', 2013, '162FMJB5031325', '821MY4B21BD200298', 'AZUL', 'NO PORTA'),
(16, 'EMPIRE', 'HORSE 150', 2012, 'KW162FMJ1950925', '812K3AC16CM049639', 'NEGRO', 'AA4I02H'),
(17, 'EMPIRE', 'HORSE 150', 2012, 'KW162FMJ1691439', '812K3AC12CM042803', 'AZUL', 'AC2B75K'),
(18, 'BERA', 'NEW JAGUAR', 2009, 'XDL162FMJ09109721', 'LDXPCKL0591A05387', 'NEGRO', 'NO PORTA'),
(19, 'EMPIRE', 'HORSE150', 2013, '8KW162FMJ2829560', '8123A1K17DM04394', 'ROJO', 'AA3Y02J'),
(20, 'BERA', 'BR 150', 2013, 'SK162FMJ1300362184', '8211MBCA8DD038276', 'GRIS', 'AE4T32G'),
(21, 'BERA', 'BR150', 2012, 'NO POSEE', '821GEDEA5CD000275', 'NEGRO', 'AB5H275'),
(22, 'BERA', 'BR150', 2012, 'NO POSEE', '8211MBCA5CD017481', 'AZUL', 'AC6J84K'),
(23, 'BERA', 'BR150', 2013, 'SK162FMJ1300360588', '8211MBCAXDD037601', 'PLATA', 'AE1573G'),
(24, 'SKIGO', 'SG-150', 2011, '162FMJC5013446', '818AM2CJ3BM221127', 'NEGRO', 'ADGF73M'),
(25, 'BERA', 'BR 150', 2012, 'YF162FMJ3CA104826', '821LMBCA8CD004059', 'ROJO', 'AFGV24D'),
(26, 'BERA', 'BR 150', 2013, '162FMJB9101617', '821LMBCA5ED100255', 'ROJO', 'AE7288D'),
(27, 'SKIGO', '150', 2009, '61FMJ91175814', 'LF3PCK0049D017666', 'ROJO', 'AC600A'),
(28, 'MD', '150', 2012, 'HJ162FMJ120444932', '8135MECA1CV008250', 'ROJO', 'AE1F88B'),
(29, 'BERA', '150', 2001, 'SK162FMJ12004158', '821LMBCA3CD046686', 'AZUL', 'AF3R61V'),
(30, 'BERA', '150', 2012, '162FMJB5103665', '821LMBCAXCD201596', 'ROJO', 'NO PORTA'),
(31, 'BERA', '200', 2012, '163FMLB5045190', '8212MCEB2CDE000198', 'AZUL', 'NO PORTA'),
(32, 'YAMAHA', '125', 2012, '505-1122545', '505-1122545', 'GRIS', 'NO PORTA'),
(33, 'NEW JAGUAR', 'BR150-2', 2009, '162FMJ95041479', 'LP6NCKB0190B03191', 'AZUL', 'AD8K790'),
(34, 'BERA', '200', 2012, '167FM8C104776', '821KMGEA3CD001438', 'NEGRO', 'AI1V34A'),
(35, 'AGUILA', '150', 2000, 'HD162FMJ111161687', '813RM9CA0CV004552', 'AZUL', 'AD9J75V'),
(36, 'BERA', '150', 2012, 'SK162FMJ1200379276', '8211MBCA1CD034276', 'NEGRO', 'A68426D'),
(37, 'BERA', '200', 2008, '163FML85026176', 'LP6PCMA0080B11228', 'ROJO', 'NO PORTA'),
(38, 'BERA', '150', 2012, '162FMJB9101843', '821LMBCA6CD100359', 'ROJO', 'NO PORTA'),
(39, 'NEW JAGUAR', 'MST150', 2008, '162FMJ280C03028', 'LEAPCK0B680C01667', 'AZUL', 'AAGA04'),
(40, 'BERA', '200', 2012, '163FMLB5045190', '8122MCEB2CDE00198', 'AZUL', 'NO PORTA'),
(41, 'SUZUKI', 'RIO150', 2006, 'BX152FMJ06100101', 'LCPMA6HE06AM7124', 'NEGRO', 'NO PORTA'),
(42, 'BERA', 'JAGUAR', 2012, '162FMJB9100064', '821LMBCA1CD200658', 'ROJO', 'AB8E44K'),
(43, 'BERA', '200', 2007, 'NO VISIBLE', 'LX8PCMKA87F002054', 'GRIS', 'NO PORTA'),
(44, 'SKYGO', 'CG 150', 2009, '161FMJ91293281', 'LF3PCKD819D006070', 'GRIS', 'AD8E30A'),
(45, 'EMPIRE', 'HORSE 150', 2009, '9601448KW163FM', '812PDK0FX9A005688', 'GRIS', 'NO PORTA'),
(46, 'BERA', '150', 2012, 'CK162FMJ1200378262', '8211MBCA7CD26540', 'GRIS', 'ACTP55K'),
(47, 'BERA', 'JAGUAR', 2009, '16284J99906208', 'L3YPCKLC99A906476', 'AZUL', 'AB9H09D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla de usuarios',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre completo del usuario',
  `login` varchar(20) NOT NULL COMMENT 'Login o nick del usuario',
  `clave` varchar(50) NOT NULL COMMENT 'Clave del usuario',
  `conf` varchar(45) NOT NULL COMMENT 'Campo de confirmación de la clave del usuario',
  `tipo` enum('Administrador','Supervisor') NOT NULL DEFAULT 'Supervisor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de usuarios del sistema' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `login`, `clave`, `conf`, `tipo`) VALUES
(1, 'Oldris Torres', 'oldrIs', '3a0cf8949de18463283d2eb72f0c00e3e4b9311a', '3a0cf8949de18463283d2eb72f0c00e3e4b9311a', 'Administrador'),
(2, 'Supervisor', 'supe', '8cd0ae8dfebb2dd4a28bffe6d2e7e3201ddf73d0', '8cd0ae8dfebb2dd4a28bffe6d2e7e3201ddf73d0', 'Supervisor');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `retenciones`
--
ALTER TABLE `retenciones`
  ADD CONSTRAINT `fk_retenciones_conductores1` FOREIGN KEY (`conductores_id`) REFERENCES `conductores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retenciones_unidades` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
